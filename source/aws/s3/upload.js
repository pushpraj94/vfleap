'use strict';

const fs = require('fs');
const zlib = require('zlib');
const AWS = require('aws-sdk');
const config = require('../config');
const debug = require('debug')('cloud-tools:s3:upload');

AWS.config.correctClockSkew = true;

/**
 * This function uploads a file to s3
 * @param bucket
 * @param key
 * @param path
 * @param contentType
 * @returns response with s3 location details
 */
function upload(bucket, key, path, contentType) {
  return new Promise((resolve, reject) => {
    debug(`Uploading file to s3 - key ${key}, path ${path}`);

    const body = fs.createReadStream(path).pipe(zlib.createGzip());

    const params = {
      Body: body,
      ACL: 'public-read',
      ContentType: contentType,
      ContentEncoding: 'gzip',
    };

    debug(`S3 upload config - ${JSON.stringify({ Bucket: bucket, Key: key })}`);
    const s3 = new AWS.S3({ params: { Bucket: bucket, Key: key } });

    s3.upload(params)
      .on('httpUploadProgress', (evt) => {
        debug('evt', evt);
      })
      .send((err, data) => {
        if (err) {
          debug('Unable upload to s3');
          return reject({
            status: config.statusCode.HTTP_404,
            message: err.message || err,
            code: config.constantsJson.errorCode.HTTP_140,
          });
        }
        debug('S3 upload success');
        return resolve(data);
      });
  });
}

module.exports = {
  upload,
};
