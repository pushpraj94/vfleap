'use strict';

const debug = require('../debug');
const aws = require('../aws');
const messageUtils = require('../message-utils');

const SQSProducer = aws.sqs.SQSProducer;
const errorTypes = messageUtils.getCommonErrorTypes();
const auditTypes = messageUtils.getCommonAuditTypes();

class Logger {
  constructor(queueUrl, region) {
    debug(`Construtucting Logger Object for queue - ${queueUrl}`);
    this.sqs = new SQSProducer(queueUrl, { region });
  }

  info(opco, message, infoDetails) {
    const template = messageUtils.getInfo(message);   
    template.OPCO = opco;
    template.INFO_DETAILS = infoDetails;
    debug(`Sending INFO logs - ${JSON.stringify(template)}`);
    return this.sqs.publish(template);
  }

  error(opco, message, errorDetails) {
    const template = messageUtils.getError(errorTypes.TYPE_ERROR, message);
    template.OPCO = opco;
    template.ERROR_DETAILS = errorDetails;
    debug(`Sending ERROR logs - ${JSON.stringify(template)}`);
    return this.sqs.publish(template);
  }

  audit(auditType, opco, message, auditDetails) {    
    const template = messageUtils.getAudit(auditTypes[auditType], message);
    template.OPCO = opco;
    template.AUDIT_DETAILS = auditDetails;
    debug(`Sending AUDIT logs - ${JSON.stringify(template)}`);
    return this.sqs.publish(template);
  }    
}

module.exports = {
  Logger,
};
