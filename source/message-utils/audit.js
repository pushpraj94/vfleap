'use strict';

const getBase = require('./base.js').getLogTemplate;
const config = require('./config.js').getConfig();

const KEYS = config.KEYS;
const VALUES = config.VALUES;

function getAudit(auditType, data) {
  const base = getBase();
  if (!auditType) {
    throw new Error(config.ERRORS.UNDEFINED_AUDIT_TYPE);
  }

  const auditAttrs = {};
  auditAttrs[KEYS.TYPE] = VALUES.TYPE.AUDIT;
  auditAttrs[KEYS.AUDIT_TYPE] = auditType;
  auditAttrs[KEYS.MESSAGE] = data;

  return Object.assign(
    {},
    base,
    auditAttrs
  );
}

function getCommonAuditTypes() {
  return config.VALUES.AUDIT_TYPE;
}

module.exports = {
  getAudit,
  getCommonAuditTypes,
};
