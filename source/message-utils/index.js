'use strict';

const init = require('./config.js').init;
const isReady = require('./config.js').isReady;
const getError = require('./error.js').getError;
const getCommonErrorTypes = require('./error.js').getCommonErrorTypes;
const getAudit = require('./audit.js').getAudit;
const getCommonAuditTypes = require('./audit.js').getCommonAuditTypes;
const getInfo = require('./info.js').getInfo;

module.exports = {
  init,
  isReady,
  getError,
  getCommonErrorTypes,
  getAudit,
  getCommonAuditTypes,
  getInfo,
};
