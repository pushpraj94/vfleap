'use strict';

const Joi = require('joi');
const _ = require('lodash');
const ctools = require('cloud-tools');
const debug = require('../debug');
const config = require('./config');

const debugName = 'VALIDATION';
const type = config.distributionType;

const schema = {
  minLatency: Joi.number().min(0).max(Joi.ref('maxLatency')).required(),
  maxLatency: Joi.number().min(Joi.ref('minLatency')).required(),
  distribution: Joi.string().required().valid(_.keys(type)),
  samples: Joi.number().min(0).required(),
};

function validateSchema(obj) {
  debug(`VALIDATION PARAMETER : ${JSON.stringify(obj)}`);
  const clone = _.cloneDeep(obj);
  const out = Joi.validate(clone, schema);
  debug('VALIDATION ERROR :', out.error ? out.error.details : null);
  return out.error;
}

module.exports = {
  validateSchema,
};
