'use strict';

const config = {
  distributionType: {
    Uniform: 'runif',
    Poisson: 'rpois',
    Beta: 'rbeta',
    Gamma: 'rgamma',
    Binomial: 'rbinom',
    Normal: 'rnorm',
    Linear: 'Linear',
    'F-Distribution': 'rexp',
  },
};

module.exports = config;
