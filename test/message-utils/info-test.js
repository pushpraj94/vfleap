'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const path = require('path');

const indexPath = path.join(__dirname, '../../source/message-utils/index.js');
const infoPath = path.join(__dirname, '../../source/message-utils/info.js');
const configPath = path.join(__dirname, '../../source/message-utils/config.js');
const basePath = path.join(__dirname, '../../source/message-utils/base.js');
const idPath = path.join(__dirname, '../../source/id/index.js');
const awsPath = path.join(__dirname, '../../source/aws/index.js');
const ec2Path = path.join(__dirname, '../../source/aws/ec2.js');

const dummyName = 'dummy_name';
const dummyEnv = 'dummy_env';
const dummyMessage = 'This is a test message';
const dummyId = '123';

describe('MessageUtils Info message Tests', function() {
  beforeEach(function() {
    delete require.cache[indexPath];
    delete require.cache[configPath];
    delete require.cache[infoPath];
    delete require.cache[basePath];
    delete require.cache[idPath];
    delete require.cache[awsPath];
    delete require.cache[ec2Path];
  });

  afterEach(function() {
    delete require.cache[indexPath];
    delete require.cache[configPath];
    delete require.cache[infoPath];
    delete require.cache[basePath];
    delete require.cache[idPath];
    delete require.cache[awsPath];
    delete require.cache[ec2Path];
  });

  it('has getInfo function', function() {
    const getInfo = require(indexPath).getInfo;
    expect(getInfo).to.be.a('function');
  });

  it('if called without initialized config throws an error', function() {
    const getInfo = require(indexPath).getInfo;
    expect(getInfo).to.throw(Error);
  });

  it('should have the correct attributes', sinon.test(function(done) {
    this.stub(require('../../source/aws/ec2.js'), 'getInstanceId', () => (
      Promise.resolve(dummyId)
    ));

    const init = require(indexPath).init;
    const getInfo = require(indexPath).getInfo;

    init(dummyName, dummyEnv)
    .then(() => {
      const msg = getInfo(dummyMessage);
      expect(msg.TIME_STAMP).to.be.an('number');
      expect(msg).to.have.property('ENVIRONMENT_NAME', dummyEnv);
      expect(msg.INSTANCE_ID.indexOf(dummyName)).to.be.equal(0);
      expect(msg).to.have.property('LOG_TYPE', 'INFO');
      expect(msg).to.have.property('MESSAGE', dummyMessage);
      done();
    })
    .catch(done);
  }));
});
